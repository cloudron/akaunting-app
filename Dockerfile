FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

RUN apt update && apt install -y php-bcmath php-zip

# Download Akaunting
RUN cd /tmp \
  && curl -sSL 'https://akaunting.com/download.php?version=latest&utm_source=cloudron&utm_campaign=developers' -o ./akaunting.zip \
  && unzip -q ./akaunting.zip -d /app/code \
  && rm ./akaunting.zip \
  && chown -R www-data:www-data /app/code

# Configure Apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
RUN a2enmod rewrite

COPY apache/akaunting.conf /etc/apache2/sites-enabled/akaunting.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

# Add Supervisor Configs
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
COPY supervisor/* /etc/supervisor/conf.d/

# Move static
RUN mv /app/code/bootstrap/cache /run/akaunting/bootstrap-cache && ln -s /run/akaunting/bootstrap-cache /app/code/bootstrap/cache \
    && mv /app/code/modules /app/data/modules && ln -s /app/data/modules /app/code/modules \
    && mv /app/code/public/files /app/data/public/files && ln -s /app/data/public/files /app/code/public/files \
    && mv /app/code/storage /app/data/storage && ln -s /app/data/storage /app/code/storage

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
