#!/bin/bash

set -eu

readonly ARTISAN="sudo -E -u www-data php /app/code/artisan"

echo "==> Startup"

mkdir -p /run/akaunting/bootstrap-cache /run/akaunting/framework-cache /run/akaunting/logs /run/akaunting/sessions

if [[ ! -f /app/data/.initialized ]]; then
    echo "==> Install Akaunting"
    $ARTISAN install \
        --db-host="${CLOUDRON_MYSQL_HOST}" \
        --db-port="${CLOUDRON_MYSQL_PORT}" \
        --db-name="${CLOUDRON_MYSQL_DATABASE}" \
        --db-username="${CLOUDRON_MYSQL_USERNAME}" \
        --db-password="${CLOUDRON_MYSQL_PASSWORD}" \
        --company-name="My Company" \
        --company-email="my@company.com" \
        --admin-email="my@company.com" \
        --admin-password="123654"

    echo "==> Update env"
    crudini --set /app/code/.env "" APP_URL "${CLOUDRON_APP_ORIGIN}"
    crudini --set /app/code/.env "" MAIL_DRIVER "smtp"
    crudini --set /app/code/.env "" MAIL_PORT "${CLOUDRON_MAIL_SMTP_PORT}"
    crudini --set /app/code/.env "" MAIL_HOST "${CLOUDRON_MAIL_SMTP_SERVER}"
    crudini --set /app/code/.env "" MAIL_USERNAME "${CLOUDRON_MAIL_SMTP_USERNAME}"
    crudini --set /app/code/.env "" MAIL_FROM_ADDRESS "${CLOUDRON_MAIL_FROM}"
    crudini --set /app/code/.env "" MAIL_PASSWORD "${CLOUDRON_MAIL_SMTP_PASSWORD}"

    echo "==> Move env"
    mv /app/code/.env /app/data/.env && ln -sf /app/data/.env /app/code/.env

    touch /app/data/.initialized
else
    echo "==> Update Akaunting"
    $ARTISAN update:all
fi

echo "==> Move cache, logs, and sessions storage to runtime"
rm -rf /app/data/storage/framework/cache && ln -s /run/akaunting/framework-cache /app/data/storage/framework/cache
rm -rf /app/data/storage/framework/sessions && ln -s /run/akaunting/sessions /app/data/storage/framework/sessions
rm -rf /app/data/storage/logs && ln -s /run/akaunting/logs /app/data/storage/logs

echo "==> Change permissions"
chown -R www-data:www-data /app/data /run

echo "=> Start Supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i akaunting

echo "==> Start Apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
