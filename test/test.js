#!/usr/bin/env node

/* global describe */
/* global before */
/* global after */
/* global it */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);


    const LOCATION = 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const EMAIL = 'my@company.com';
    const PASSWORD = '123654';
    const MAILBOX_EMAIL = 'test@cloudron.io';
    const MAILBOX_NAME = 'cloudrontest';

    var browser, app;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    function waitForElement(elem) {
        return browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
        });
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    function login(callback) {
        browser.get(`https://${app.fqdn}/auth/login`).then(function () {
            return browser.findElement(By.name('email')).sendKeys(EMAIL);
        }).then(function () {
            return browser.findElement(By.name('password')).sendKeys(PASSWORD);
        }).then(function () {
            return browser.findElement(By.xpath('//form')).submit();
        }).then(function () {
            return waitForElement(By.xpath('//div[text()="Dashboard"]'));
        }).then(function () {
            callback();
        });
    }

    function logout(callback) {
        browser.get(`https://${app.fqdn}`).then(function () {
            return waitForElement(By.xpath('//div[text()="Dashboard"]'));
        }).then(function () {
            return waitForElement(By.xpath(`//a[@href="https://${app.fqdn}/auth/logout"]`));
        }).then(function () {
            return browser.findElement(By.xpath(`//a[@href="https://${app.fqdn}/auth/logout"]`)).click();
        }).then(function () {
            return waitForElement(By.name('password'));
        }).then(function () {
            callback();
        });
    }

    function mailboxExists(callback) {
        browser.get(`https://${app.fqdn}`).then(function () {
            return waitForElement(By.xpath(`//div[@class="dash-card-content"]//a[text()="${MAILBOX_EMAIL}"]`));
        }).then(function () {
            return waitForElement(By.xpath(`//div[@class="dash-card-content"]//a[text()="${MAILBOX_NAME}"]`));
        }).then(function () {
            callback();
        });
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can create mailbox', function (callback) {
        browser.get(`https://${app.fqdn}/mailbox/new`).then(function () {
            return browser.findElement(By.id('email')).sendKeys(MAILBOX_EMAIL);
        }).then(function () {
            return browser.findElement(By.id('name')).sendKeys(MAILBOX_NAME);
        }).then(function () {
            return browser.findElement(By.xpath('//div[@class="wizard-body"]//form')).submit();
        }).then(function () {
            return waitForElement(By.id('aliases'));
        }).then(function () {
            callback();
        });
    });
    it('mailbox exists', mailboxExists);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('can login', login);
    it('mailbox exists', mailboxExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () { execSync(`cloudron restore --app ${app.id}`, EXEC_ARGS); });

    it('can login', login);
    it('mailbox exists', mailboxExists);
    it('can logout', logout);

    it('move to different location', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('mailbox exists', mailboxExists);
    it('can logout', logout);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    // TODO update tests will come once we have an update
});
