# Akaunting Cloudron App

This repository contains the Cloudron app package source for [Akaunting](https://akaunting.com).

## Installation

[![Install](https://cloudron.io/img/button32.png)](https://cloudron.io/button.html?app=com.akaunting.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id com.akaunting.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd akaunting-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, backup and restore.

```
cd akaunting-app/test

npm install
mocha test.js
```
