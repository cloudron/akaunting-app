This app packages latest version of Akaunting

Akaunting is a free, open source and online accounting software designed for small businesses and freelancers. It is built with modern technologies such as Laravel, VueJS, Bootstrap 4, RESTful API etc. Thanks to its modular structure, Akaunting provides an awesome App Store for users and developers.

From invoicing to expense tracking to accounting, Akaunting has all the tools you need to manage your money online, for free.

Get invoices paid. Track expenses.
